package com;


import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

@RestController
public class Controller
{

    @GetMapping("/home")
public String[] getFiles(){

    String[] pathnames;

    // Creates a new File instance by converting the given pathname string
    // into an abstract pathname
    File f = new File("src/main/resources/pdfs");

    // Populates the array with names of files and directories
    pathnames = f.list();

    // For each pathname in the pathnames array
    for (String pathname : pathnames) {
        // Print the names of files and directories
        System.out.println(pathname);
    }
    return pathnames;
}

    @GetMapping("/home2")
        public String getLinkToFile() throws Exception {
            String body =
                    "<HTML><body> <a href=\"file\">Europa Versicherung Diverse.pdf</a></body></HTML>";
            return (body);

        }

    @GetMapping( "/file")
    @ResponseBody
    public FileSystemResource getFile() {
        return new FileSystemResource("/Users/alexander.kracht/IdeaProjects/PDF_TagReader/src/main/resources/pdfs/Europa Versicherung Diverse.pdf");
    }

    @GetMapping( "/file2")
    @ResponseBody
    public void getFile2() throws IOException {
        //String file = "/Users/alexander.kracht/IdeaProjects/PDF_TagReader/src/main/resources/pdfs/LoremIpsum.pdf";
        File file = new File( "/Users/alexander.kracht/IdeaProjects/PDF_TagReader/src/main/resources/pdfs/LoremIpsum.pdf");

        //Runtime.getRuntime().exec(String.valueOf(file));
        //Desktop.getDesktop().open(file);
        if (file.toString().endsWith(".pdf"))
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
        else {
            Desktop desktop = Desktop.getDesktop();
            desktop.open(file);
        }


    }




/*    @GetMapping( "/files/{file_name}")
    @ResponseBody
    public FileSystemResource getFile(@PathVariable("file_name") String fileName) {
        return new FileSystemResource(myService.getFileFor(fileName));
    }*/

    }

