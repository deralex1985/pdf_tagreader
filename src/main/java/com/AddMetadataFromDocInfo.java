package com;

import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import javax.xml.transform.TransformerException;

public final class AddMetadataFromDocInfo
{


    public static void main( String[] args ) throws IOException, TransformerException
    {

            File inputFile= new File("src/main/resources/Europa Versicherung Diverse.pdf");
             //File inputFile= new File("/../resources/pdfs/Europa Versicherung Diverse.pdf");
             PDDocument document = PDDocument.load(inputFile);

                String tagsString= document.getDocumentInformation().getKeywords();
                List<String> tags = Arrays.asList(tagsString.split(","));
                for (String tag : tags) {
                    String pathTagFolder = "src/main/resources/"+tag.trim();
                    if (Files.notExists(Paths.get(pathTagFolder)))
                    {
                        System.out.println("Nöö");
                        File newTagFolder = new File(pathTagFolder);
                        //Creating the directory
                        boolean bool = newTagFolder.mkdir();
                    }

                System.out.println (tag);
                }



            }

}